import configurations.opencommunity as occonf
import configurations.squadjoins as sqconf
from configurations.baseconf import BaseConf, BaseSwitch
from settings import Settings


class Switch(BaseSwitch):
    def __init__(self):
        super().__init__()
        self.add_tab(Conf())
        self.add_tab(sqconf.Conf())
        self.add_tab(occonf.Conf())


class Conf(BaseConf):
    name = 'Crimson'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['pre-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['wurmspins'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['burn'])
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['post-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['wurmhead'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['broadcasts'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['aftermath'])
        self._calculate_choices()
