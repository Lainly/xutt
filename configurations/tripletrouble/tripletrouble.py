import configurations.tripletrouble.cobalt as cobaltconf
import configurations.tripletrouble.crimson as crimsonconf
import configurations.tripletrouble.orga_pre as orgapreconf

import configurations.opencommunity as occonf
import configurations.squadjoins as sqconf
import configurations.tripletrouble.amber as amberconf
import configurations.tripletrouble.orga as orgaconf
from configurations.baseconf import BaseSwitch


class Switch(BaseSwitch):
    def __init__(self):
        super().__init__()
        self.add_tab(amberconf.Conf())
        self.add_tab(cobaltconf.Conf())
        self.add_tab(crimsonconf.Conf())
        self.add_tab(occonf.Conf())
        self.add_tab(orgaconf.Conf())
        self.add_tab(orgapreconf.Conf())
        self.add_tab(sqconf.Conf())



