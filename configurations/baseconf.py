import re
import json
import collections


class BaseSwitch:
    def __init__(self):
        self.tabs = []

    def get_tabinfos(self):
        return self.tabs

    def get_tab(self, name):
        for tab in self.tabs:
            if tab.name == name:
                return tab
        return BaseConf()

    def add_tab(self, conf):
        self.tabs.append(conf)


class BaseConf:
    name = "BaseConf"

    def __init__(self):
        self.choices = {}
        self.ordered_choices = []
        self.json_source = collections.OrderedDict()
        self.templ_replacements = []
        self.templ_start_seq = '{{'
        self.templ_end_seq = '}}'

    def get_data(self):
        return self.choices, self.ordered_choices

    def _load_json(self, file_path, data_mod_func=None, data_sort_func=None, calculate_choices=True):
        # Check if modifier functions are set. If not: set default, passthrough functions
        if data_mod_func is None:
            data_mod_func = self._json_data_modifier

        if data_sort_func is None:
            data_sort_func = self._json_data_sort

        with open(file_path, 'r') as json_file:
            # Load json file as OrderedDict to preserve the order of the entries
            loaded_json = json.load(json_file, object_pairs_hook=collections.OrderedDict)

            for part in loaded_json:
                # Pipe all entries through the modifier and templating functions.
                # The modifier functions can create new entries (1 entry => 3 entries e.g.)!
                modified_entries = collections.OrderedDict()

                for entry in loaded_json[part]:
                    loaded_json[part][entry] = self.template_replace(loaded_json[part][entry], loaded_json)
                    modified_entries.update(data_mod_func(loaded_json[part][entry], original_entry_key=entry))

                # Overwrite all entries in this part with the modified entries.
                loaded_json[part] = modified_entries

            # Sort entries if wished.
            loaded_json = data_sort_func(loaded_json)

            # The same part name can be composed from multiple files, so we need to extend existing parts here
            # (instead of overwriting them)
            for part in loaded_json.keys():
                if part in self.json_source.keys():
                    self.json_source[part].update(loaded_json[part])
                else:
                    self.json_source[part] = loaded_json[part]

        if calculate_choices:
            self._calculate_choices()

    def _calculate_choices(self):
        """
        Basically cache all the choices into a separate value, instead of re-calculating/parsing them every time 
        the conf gets activated.
        """
        self.ordered_choices = []
        self.choices = {}

        for part in self.json_source:
            for entry in self.json_source[part]:
                key = '{part}.{entry_key}'.format(part=part, entry_key=entry)
                self.ordered_choices.append(key)
                self.choices[key] = self.json_source[part][entry]

    @staticmethod
    def _json_data_modifier(entry, original_entry_key):
        modified_entries = collections.OrderedDict()
        modified_entries[original_entry_key] = entry
        return modified_entries

    @staticmethod
    def _json_data_sort(json_data):
        return json_data

    @staticmethod
    def _find_leaf(key, loaded_json):
        """
        Finds value in dictionary object based on key in string form.
        :param key: the key, parts separated by dots (e.g. section.entry.text)
        :param loaded_json: the dictionary object from which the value should be taken
        :return: associated value of key
        """
        hold = loaded_json.copy()
        for part in key.split('.'):
            hold = hold[part]
        return hold

    def template_replace(self, entry, loaded_json):
        for key, value in entry.items():
            for sr in re.findall(f'{self.templ_start_seq}key:(?P<dkey>.*?){self.templ_end_seq}', value):
                entry[key] = entry[key].replace(f'{self.templ_start_seq}key:{sr}{self.templ_end_seq}',
                                                self._find_leaf(sr, loaded_json))
                pass

            for old, new in self.templ_replacements:
                    entry[key] = value.replace(f'{self.templ_start_seq}{old}{self.templ_end_seq}', f'{new}')

        return entry
