from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'Orga (Pre)'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['shatterer']['orga-pre'])
        self._calculate_choices()
