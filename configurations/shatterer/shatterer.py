import configurations.shatterer.orga_pre as orgapreconf
import configurations.opencommunity as occonf
import configurations.squadjoins as sqconf

import configurations.shatterer.orga as orgaconf
from configurations.baseconf import BaseConf, BaseSwitch
from settings import Settings


class Switch(BaseSwitch):
    def __init__(self):
        super().__init__()
        self.add_tab(Conf())
        self.add_tab(occonf.Conf())
        self.add_tab(orgaconf.Conf())
        self.add_tab(orgapreconf.Conf())
        self.add_tab(sqconf.Conf())


class Conf(BaseConf):
    name = 'Shatterer'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['shatterer']['shatterer'])
        self._calculate_choices()







