from configurations.shatterer.shatterer import Switch as tsswitch
import configurations.html_about as htmlaboutconf


class Switch(tsswitch):
    def __init__(self):
        super().__init__()
        self.add_tab(htmlaboutconf.Conf())