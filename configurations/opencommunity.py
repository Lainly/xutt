from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'OC'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['oc-links'])
        self._calculate_choices()

