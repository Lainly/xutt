from configurations.orga_pre_templated import Conf as templatedOrga
from settings import Settings


class Conf(templatedOrga):
    name = 'Orga (Pre)'

    def __init__(self):
        super().__init__(replacements=[('mapname', 'Auric Basin')])
        #self._load_json(Settings.json_sources['tarir']['orga-pre'])
        self._calculate_choices()
