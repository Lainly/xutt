import configurations.html_about as htmlaboutconf
from configurations.tarir.tarir import Switch as tswitch


class Switch(tswitch):
    def __init__(self):
        super().__init__()
        self.add_tab(htmlaboutconf.Conf())