from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'Orga'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['tarir']['orga'])
        self._load_json(Settings.json_sources['orga'])
        self._calculate_choices()
