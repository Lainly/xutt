from configurations.dragonstand.dragonstand import Switch as dsswitch
import configurations.html_about as htmlaboutconf


class Switch(dsswitch):
    def __init__(self):
        super().__init__()
        self.add_tab(htmlaboutconf.Conf())