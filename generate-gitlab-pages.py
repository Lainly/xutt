import canni

if __name__ == '__main__':
    shg = canni.StaticHtmlGenerator(output_directory='gitlab-pages')
    shg.render_switch('tripletrouble.tripletrouble_html', output_filename='tt/tripletrouble.html',
                      pagetitle='Triple Trouble - OpenCommunity Style')
    shg.render_switch('tripletrouble.amber', output_filename='tt/amber.html', pagetitle='Amber Wurm - Triple Trouble')
    shg.render_switch('tripletrouble.cobalt', output_filename='tt/cobalt.html', pagetitle='Cobalt Wurm - Triple Trouble')
    shg.render_switch('tripletrouble.crimson', output_filename='tt/crimson.html', pagetitle='Crimson Wurm - Triple Trouble')


    shg.render_switch('shatterer.shatterer_html', output_filename='st/shatterer.html',
                      pagetitle='Shatterer - OpenCommunity Style')

    shg.render_switch('tarir.tarir_html', output_filename='hot/tarir.html',
                      pagetitle='Tarir / Auric Basin Meta - OpenCommunity Style')
    shg.render_index(output_filename='index.html', pagetitle='Copypasta Index - OpenCommunity Style')
