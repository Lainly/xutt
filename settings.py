class Settings:
    keys = {
        'select': ('return', 'enter'),
        'escape': ('esc', 'q', 'Q'),
        'up': ('up', 'k'),
        'down': ('down', 'j'),
        'home': ('0', '^'),
        'end': ('$',),
    }

    json_sources = {
        'tripletrouble': {
            'cobalt': {
                'pre-burn': './json_sources/tripletrouble/cobalt/cobalt_pre-burn.json',
                'post-burn': './json_sources/tripletrouble/cobalt/cobalt_post-burn.json',
                'escort': './json_sources/tripletrouble/cobalt/cobalt_escort.json',
                'broadcasts': './json_sources/tripletrouble/cobalt/cobalt_broadcasts.json',
            },
            'amber': {
                'pre-burn': './json_sources/tripletrouble/amber/amber_pre-burn.json',
                'post-burn': './json_sources/tripletrouble/amber/amber_post-burn.json',
                'escort': './json_sources/tripletrouble/amber/amber_escort.json',
                'broadcasts': './json_sources/tripletrouble/amber/amber_broadcasts.json',
            },
            'crimson': {
                'pre-burn': './json_sources/tripletrouble/crimson/crimson_pre-burn.json',
                'post-burn': './json_sources/tripletrouble/crimson/crimson_post-burn.json',
                'escort': './json_sources/tripletrouble/crimson/crimson_escort.json',
                'broadcasts': './json_sources/tripletrouble/crimson/crimson_broadcasts.json',
            },
            'dryrun': {
                'wurmspins': './json_sources/tripletrouble/dryrun_general/wurmspins.json',
                'burn': './json_sources/tripletrouble/dryrun_general/burn.json',
                'wurmhead': './json_sources/tripletrouble/dryrun_general/wurmhead.json',
                'escort': './json_sources/tripletrouble/dryrun_general/escort.json',
                'aftermath': './json_sources/tripletrouble/dryrun_general/aftermath.json',
            },
            'orga': './json_sources/tripletrouble/orga.json',
            'orga-pre': './json_sources/tripletrouble/orga-pre.json',
            # commander specific
            'layniar': {
                'cobalt-full': './json_sources/tripletrouble/cobalt/layniar/layniar_cobalt_full.json',
            },
        },
        'shatterer': {
            'orga': './json_sources/shatterer/orga.json',
            'orga-pre': './json_sources/shatterer/orga-pre.json',
            'shatterer': './json_sources/shatterer/shatterer.json'
        },
        'tarir': {
            'orga': './json_sources/tarir/orga.json',
            'tarir': './json_sources/tarir/tarir.json'
        },
        'dragonstand': {

        },
        'oc-links': './json_sources/opencommunity_links.json',
        'oc-squadjoins': './json_sources/opencommunity_squadjoins.json',
        'orga': './json_sources/orga.json',
        'orga-pre': './json_sources/orga-pre.json',
        'html-about': './json_sources/html-about.json',
    }

    scroll_scaling = {
        'wheel-up': ['up' for x in range(5)],
        'wheel-down': ['down' for x in range(5)],
        'key-up': ['up'],
        'key-down': ['down'],
    }