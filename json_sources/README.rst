How to write json sources
=========================

.. code-block:: json

    {
      "general_section_key_without_dots": {
        "first_specific_entry_key_without_dots": {
          "text": "text which should be copied into the clipboard",
          "title": "title which is displayed in the ui"
        },
        "second_specific_entry_key_without_dots": {
          "text": "text which should be copied into the clipboard",
          "title": "title which is displayed in the ui"
        }
      }
    }